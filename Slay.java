import gui.Frame;

public final class Slay {
	private static final long serialVersionUID = 1L;
	private static final String DEVELOP = "Develop";

	public static void main(String[] args) {
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			if (DEVELOP.equals(arg)) {
				Frame.develop = true;
			}
		}
		Frame frame = new Frame();
		frame.setVisible(true);
	}
}
