package gui;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * MoveBar.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
final class MoveBar extends Canvas implements MouseListener {

	private static final long serialVersionUID = 1L;

	private final BuyWindow _buyWindow;
	private final Rectangle _close = new Rectangle();

	MoveBar(BuyWindow buyWindow) {
		super();
		_buyWindow = buyWindow;
		addMouseListener(this);
		setBackground(Color.BLUE);
	}

	public void mouseClicked(MouseEvent event) {
		Point pt = event.getPoint();
		if (_close.contains(pt)) {
			_buyWindow.close();
		}
	}

	public void mouseEntered(MouseEvent arg0) {
	}

	public void mouseExited(MouseEvent arg0) {
	}

	public void mousePressed(MouseEvent arg0) {
	}

	public void mouseReleased(MouseEvent arg0) {
	}

	public void paint(Graphics graphics) {
		Dimension dim = getSize();
		_close.setBounds(dim.width - dim.height, 0, dim.height - 1, dim.height - 1);

		graphics.setColor(Color.WHITE);
		graphics.fillRect(_close.x, _close.y, _close.width, _close.height);
		graphics.setColor(Color.BLACK);
		graphics.drawRect(_close.x, _close.y, _close.width, _close.height);

		graphics.drawLine(_close.x, _close.y, _close.x + _close.width, _close.y + _close.height);
		graphics.drawLine(_close.x, _close.y + _close.height, _close.x + _close.width, _close.y);
	}

}
