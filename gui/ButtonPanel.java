package gui;

import java.awt.Button;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * ButtonPanel.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
final class ButtonPanel extends Panel implements ActionListener {

	private static final String NEW_SMALL_GAME = "New Small Game";
	private static final String NEW_MEDIUM_GAME = "New Medium Game";
	private static final String NEW_LARGE_GAME = "New Large Game";
	private static final String END_TURN = "End Turn";
	private static final String RESTART_GAME = "Restart Game";

	private static final String[] LABELS = new String[] { NEW_SMALL_GAME, NEW_MEDIUM_GAME,
			NEW_LARGE_GAME, RESTART_GAME, END_TURN };

	private static final long serialVersionUID = 1L;

	private final DetailPanel _detailPanel;
	private final Button[] _button;

	ButtonPanel(DetailPanel detailPanel) {
		super(new GridLayout(-1, 1));
		_detailPanel = detailPanel;

		_button = new Button[LABELS.length];
		for (int i = 0; i < LABELS.length; i++) {
			_button[i] = new Button(LABELS[i]);
			_button[i].addActionListener(this);
			add(_button[i]);
		}
		disableGUI();
	}

	public void actionPerformed(ActionEvent event) {
		final String cmd = event.getActionCommand();
		if (END_TURN.equals(cmd)) {
			_detailPanel.endHumanTurn();
		} else if (RESTART_GAME.equals(cmd)) {
			_detailPanel.restartGame();
		} else if (NEW_SMALL_GAME.equals(cmd)) {
			_detailPanel.newSmallGame();
		} else if (NEW_MEDIUM_GAME.equals(cmd)) {
			_detailPanel.newMediumGame();
		} else if (NEW_LARGE_GAME.equals(cmd)) {
			_detailPanel.newLargeGame();
		}
	}

	final void disableGUI() {
		for (int i = 3; i < LABELS.length; i++) {
			_button[i].setEnabled(false);
		}
	}

	/**
	 * 
	 */
	final void enableGui() {
		for (int i = 0; i < LABELS.length; i++) {
			_button[i].setEnabled(true);
		}
	}

}
