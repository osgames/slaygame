/**
 * 
 */
package gui;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.File;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * FileManager.java is part of Phill van Leersum's SlayGame program.
 * 
 * SlayGame is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * SlayGame is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * SlayGame. If not, see http://www.gnu.org/licenses/.
 * 
 */
/**
 * 
 */
public class FileManager {
	private String _currentDirectory = ".";
	private String _extension = ".slay";

	public final File getLoadFile(Frame frame) {
		File file = null;
		FileDialog fd = new FileDialog(frame, "Load File", FileDialog.LOAD);
		// java.io.File curdir = new java.io.File(".");
		fd.setDirectory(_currentDirectory);
		fd.setFile("*" + _extension);
		fd.setVisible(true);
		String filename = fd.getFile();
		String directoryName = fd.getDirectory();
		if (null != filename && null != directoryName) {
			_currentDirectory = directoryName;
			String currentFileName = directoryName + filename;
			file = new File(currentFileName);
		}
		return file;
	}

	public final File getSaveFile(Frame frame) {
		File file = null;
		FileDialog fd = new FileDialog(frame, "Save File", FileDialog.SAVE);
		// java.io.File curdir = new java.io.File(".");
		fd.setDirectory(_currentDirectory);
		fd.setFile("*" + _extension);
		fd.setVisible(true);
		String filename = fd.getFile();
		String directoryName = fd.getDirectory();
		if (null != filename && null != directoryName) {
			if (!filename.endsWith(_extension)) {
				filename = filename.concat(_extension);
			}
			_currentDirectory = directoryName;
			String currentFileName = directoryName + filename;
			file = new File(currentFileName);
		}
		return file;
	}

}
