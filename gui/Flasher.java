package gui;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Flasher.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Simple thread to invoke the flash routine in PlayView
 */
final class Flasher extends Thread {

	private final PlayView _playView;
	private boolean _flash = false;

	Flasher(PlayView playView) {
		super();
		_playView = playView;
	}

	public void run() {
		while (true) {
			synchronized (this) {
				try {
					wait(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			_flash = !_flash;
			_playView.paintFlash(_flash);
			Thread.yield();
		}
	}
}
