package gui.assess;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Panel;

public class AssessDetail extends Panel {

	private static final long serialVersionUID = 1L;
	private final AssessmentPanel _panel;
	private final AssessButtonPanel _buttonPanel;

	public AssessDetail(AssessmentPanel panel) {
		super(new BorderLayout());
		_panel = panel;
		_buttonPanel = new AssessButtonPanel(this);
		add(BorderLayout.NORTH, _buttonPanel);
	}

	public final Dimension getPreferredSize() {
		return new Dimension(150, 0);
	}

	public void setPlayer(int player) {
		_panel.setPlayer(player);
	}

}
