package gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.Point;

import model.Territory;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * DetailPanel.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Contains/organises the GUI elements on the right of the screen
 */
final class DetailPanel extends Panel {
	private static final long serialVersionUID = 1L;

	private final ButtonPanel _buttonPanel;
	private final RankingPanel _rankings = new RankingPanel();
	private final SlayPanel _slayPanel;
	private final SoldierPanel _soldierPanel;
	private Territory _territory = null;

	private final TownDetailPanel _townPanel;

	DetailPanel(SlayPanel slayPanel) {
		super(new GridLayout(-1, 1));
		_slayPanel = slayPanel;

		_buttonPanel = new ButtonPanel(this);
		_soldierPanel = new SoldierPanel(this);
		_townPanel = new TownDetailPanel();
		add(_buttonPanel);
		add(_soldierPanel);
		add(_townPanel);
		add(_rankings);

	}

	/**
	 * @param pt
	 *            absolute location
	 */
	final void buyCaptain(Point pt) {
		if (null != _territory) {
			_slayPanel.buyCaptain(_territory, pt);
		}
	}

	/**
	 * @param pt
	 *            absolute location
	 */
	final void buyCastle(Point pt) {
		if (null != _territory) {
			_slayPanel.buyCastle(_territory, pt);
		}
	}

	/**
	 * @param pt
	 *            absolute location
	 */
	final void buyColonel(Point pt) {
		if (null != _territory) {
			_slayPanel.buyColonel(_territory, pt);
		}
	}

	/**
	 * @param pt
	 *            absolute location
	 */
	final void buyMajor(Point pt) {
		if (null != _territory) {
			_slayPanel.buyMajor(_territory, pt);
		}
	}

	/**
	 * @param pt
	 *            absolute location
	 */
	final void buyPrivate(Point pt) {
		if (null != _territory) {
			_slayPanel.buyPrivate(_territory, pt);
		}
	}

	final void disableGUI() {
		_buttonPanel.disableGUI();
	}

	final void enableGui() {
		_buttonPanel.enableGui();
	}

	final void endHumanTurn() {
		_slayPanel.endHumanTurn();
	}

	final int getMoney() {
		return _townPanel.getMoney();
	}

	public final Dimension getPreferredSize() {
		return new Dimension(150, 0);
	}

	final TownDetailPanel getTownPanel() {
		return _townPanel;
	}

	final void newLargeGame() {
		_slayPanel.newLargeGame();
	}

	final void newMediumGame() {
		_slayPanel.newMediumGame();
	}

	final void newSmallGame() {
		_slayPanel.newSmallGame();
	}

	public void refresh() {
		_townPanel.refresh();
		_soldierPanel.refresh();
	}

	final void restartGame() {
		_slayPanel.restartGame();
	}

	final void setMove(int moveNumber) {
		_townPanel.setMove(moveNumber);
	}

	final void setRankings(int[] rankings) {
		_rankings.setRankings(rankings);
	}

	final void setTerritory(Territory territory) {
		_territory = territory;
		_townPanel.setTerritory(_territory);
		_soldierPanel.repaint();
	}
}
