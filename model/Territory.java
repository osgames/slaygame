package model;

import java.util.ArrayList;

public final class Territory extends Fn {

	private boolean _current = false;
	private final ArrayList _hexagons = new ArrayList();
	private final int _owner;
	private Hexagon _townLocation = null;
	private final Board _board;

	public Territory(Board board, Hexagon hex) {
		super();
		_board = board;
		// if (null != hex.getTerritory()) {
		// throw new Error("Hex already has territory");
		// }
		_owner = hex.getOwner();
		if (NONE == _owner) {
			throw new Error("Unowned hex");
		}
		// add the hexagon, which will recursively add all mmberss of the
		// territory
		add(hex);
		// at this point we may hhave a town or not
		ensureTown();
	}

	private void ensureTown() {
		for (int t = 0; t < _hexagons.size() && null == _townLocation; t++) {
			Hexagon lookat = (Hexagon) _hexagons.get(t);
			int occ = lookat.getOccupant();
			if (isTown(occ)) {
				_townLocation = lookat;
			}
		}
		if (null == _townLocation) {
			// no town, so add a new one - find an empty hex
			Hexagon placeTown = null;
			for (int t = 0; t < _hexagons.size() && null == placeTown; t++) {
				Hexagon lookat = (Hexagon) _hexagons.get(t);
				if (NONE == lookat.getOccupant()) {
					placeTown = lookat;
				}
			}

			// couldnt find empty hex - choose one at random
			// TODO select least important occupant for replacement
			if (null == placeTown) {
				placeTown = (Hexagon) Random.get(_hexagons);
			}
			_townLocation = placeTown;
			placeTown.setOccupant(TOWN);
		}
	}

	/**
	 * Add given hex, and recursively add all contiguous hexes with the same
	 * owner
	 * 
	 * @param hex
	 */
	private final void add(Hexagon hex) {
		addOneHex(hex);
		addHexesNeighbours(hex);
	}

	/**
	 * Add the given hex.
	 * 
	 * @param hex
	 */
	private void addHexesNeighbours(Hexagon hex) {
		Hexagon[] neighbours = hex.getNeighbours();
		for (int i = 0; i < neighbours.length; i++) {
			Hexagon neighbour = neighbours[i];
			if (null != neighbour) {
				if (!_hexagons.contains(neighbour)) {
					if (_owner == neighbour.getOwner()) {
						// if (neighbour.getTerritory() == null) {
						add(neighbour);
						// }
					}
				}
			}
		}
	}

	void addOneHex(Hexagon hex) {
		int occupant = hex.getOccupant();
		if (isTown(occupant)) {
			if (null == _townLocation) {
				_townLocation = hex;
			} else {
				// already have a town
				int newTown = occupant;
				int oldTown = _townLocation.getOccupant();
				int newTownMoney = getMoney(newTown);
				int oldTownMoney = getMoney(oldTown);
				Hexagon oldTownLoc = _townLocation;
				if (newTownMoney > oldTownMoney) {
					_townLocation = hex;
					if (null == oldTownLoc) {
						new Error("Why null oldTownLoc").printStackTrace();
					} else {
						oldTownLoc.setOccupant(NONE);
					}

				} else {
					hex.setOccupant(NONE);
				}
				setMoney(oldTownMoney + newTownMoney);
			}
		}
		_hexagons.add(hex);
		hex.setTerritory(this);
	}

	final void amalgamate(Territory territory) {
		// amalgamate territories
		ArrayList gainHexes = territory.getHexagons();
		for (int i = 0; i < gainHexes.size(); i++) {
			Hexagon gain = (Hexagon) gainHexes.get(i);
			addOneHex(gain);
			gain.setOwner(_owner);
		}
		territory.empty();
	}

	public final void beginTurn() {
		// remove graves
		for (int i = 0; i < _hexagons.size(); i++) {
			Hexagon loc = (Hexagon) _hexagons.get(i);
			int occ = loc.getOccupant();
			if (isGrave(occ)) {
				loc.setOccupant(NONE);
			}
		}

		int money = getMoney();
		money += size();
		money -= getExpenses();

		if (money < 0) {
			// starvation....
			for (int i = 0; i < _hexagons.size(); i++) {
				Hexagon loc = (Hexagon) _hexagons.get(i);
				int occ = loc.getOccupant();
				if (isSoldier(occ)) {
					loc.setOccupant(GRAVE);
				}
			}
			money = 0;
		} else {
			// set soldiers moveable
			for (int i = 0; i < _hexagons.size(); i++) {
				Hexagon loc = (Hexagon) _hexagons.get(i);
				int occ = loc.getOccupant();
				if (NONE != occ && isSoldier(occ)) {
					occ = setMoveable(occ, true);
					loc.setOccupant(occ);
				}
			}
		}
		setMoney(money);
	}

	public final void buyMilitary(int type, Hexagon hexagon) {
		boolean success = false;
		int rank = rankOf[type];
		if (_hexagons.contains(hexagon)) {
			// space is ours
			int occ = hexagon.getOccupant();
			if (NONE == occ) {
				// empty sppace
				success = true;
			} else if (isSoldier(occ)) {
				// can we combine?
				int occType = getType(occ);
				int newRank = rank + rankOf[occType];
				success = newRank <= rankOf[COLONEL];
			}
		} else if (hexagon.isAdjacentTo(_hexagons)) {
			// space is not ours but we can reach it
			if (rank > hexagon.effectiveRank()) {
				// we have the rank to take it
				success = true;
			}
		}

		if (success) {
			int money = getMoney();
			if (money >= costOf[type]) {
				money -= costOf[type];
				setMoney(money);
				int military = setMoveable(type, true);
				moveTo(military, hexagon);
			}
		}
	}

	private final void empty() {
		_hexagons.clear();
		_current = false;
		_townLocation = null;
	}

	public final int getExpenses() {
		int expenses = 0;
		for (int i = 0; i < _hexagons.size(); i++) {
			Hexagon loc = (Hexagon) _hexagons.get(i);
			int occ = loc.getOccupant();
			if (NONE != occ) {
				if (isSoldier(occ)) {
					int type = getType(occ);
					expenses += payOf[type];
				}
			}
		}
		return expenses;
	}

	public final ArrayList getHexagons() {
		return _hexagons;
	}

	public int getMoney() {
		// if (null != _townLocation) {
		int town = _townLocation.getOccupant();
		return getMoney(town);
		// }
		// return 0;
	}

	public final Player getOwner() {
		return _board.getPlayer(_owner);
	}

	public final boolean hasArmy() {
		boolean hasArmy = false;
		for (int i = 0; !hasArmy && i < _hexagons.size(); i++) {
			Hexagon loc = (Hexagon) _hexagons.get(i);
			int occ = loc.getOccupant();
			if (isSoldier(occ)) {
				hasArmy = true;
			}
		}
		return hasArmy;
	}

	public boolean isCurrent() {
		return _current;
	}

	public boolean moveTo(int moveOcc, Hexagon dstHex) {
		if (isGrave(moveOcc)) {
			return false;
		}
		int dstOccupant = dstHex.getOccupant();
		boolean success = false;
		if (isTown(moveOcc)) {
			if (_hexagons.contains(dstHex)) {
				// can place on own territory? yes if it is empty
				if (NONE == dstOccupant || isGrave(dstOccupant)) {
					success = true;
					dstHex.setOccupant(moveOcc);
					_townLocation = dstHex;
				}
			}
		} else if (isCastle(moveOcc)) {
			if (_hexagons.contains(dstHex)) {
				// can place on own territory? yes if it is empty
				if (NONE == dstOccupant || isGrave(dstOccupant)) {
					success = true;
					dstHex.setOccupant(moveOcc);
				}
			}
		} else if (isSoldier(moveOcc)) {
			if (_hexagons.contains(dstHex)) {
				if (NONE == dstOccupant || isGrave(dstOccupant)) {
					success = true;
					dstHex.setOccupant(moveOcc);
				} else if (isSoldier(dstOccupant)) {
					int newSoldier = NONE;
					int newRank = rankOf[getType(dstOccupant)];
					newRank += rankOf[getType(moveOcc)];
					if (newRank == rankOf[CAPTAIN]) {
						newSoldier = CAPTAIN;
					} else if (newRank == rankOf[MAJOR]) {
						newSoldier = MAJOR;
					} else if (newRank == rankOf[COLONEL]) {
						newSoldier = COLONEL;
					}
					if (NONE != newSoldier) {
						boolean newSoldierMoveable = isMoveable(moveOcc);
						boolean dstOccuppantMoveable = isMoveable(dstOccupant);
						newSoldier = setMoveable(newSoldier, newSoldierMoveable
								&& dstOccuppantMoveable);
						success = true;
						dstHex.setOccupant(newSoldier);
					}
				} else if (isTown(dstOccupant)) {
					if (Settings.allowRetire) {
						success = true;
						int money = getMoney() + costOf[getType(moveOcc)];
						setMoney(money);
					}
				}
			} else if (dstHex.isAdjacentTo(_hexagons)) {
				int moveOccRank = rankOf[getType(moveOcc)];
				boolean attackable = moveOccRank > dstHex.effectiveRank();
				if (attackable) {
					success = true;
					moveOcc = setMoveable(moveOcc, false);
					Player owner = getOwner();
					dstHex.capture(this, moveOcc, owner.getId());
				}
			}
		}
		return success;
	}

	final void setCurrent(boolean current) {
		_current = current;
	}

	public void setMoney(int money) {
		// if (null != _townLocation) {
		int town = _townLocation.getOccupant();
		town = setMoney(town, money);
		_townLocation.setOccupant(town);
		// }
	}

	public final int size() {
		return _hexagons.size();
	}

	public void remove(Hexagon lost) {
		_hexagons.remove(lost);
		// check that we are valid
		// first reset initial conditions
		ArrayList originalHexes = new ArrayList(_hexagons);
		_hexagons.clear();
		_townLocation = null;

		// then rebuild the territory
		Hexagon hex = (Hexagon) originalHexes.get(0);
		add(hex);
		ensureTown();
		originalHexes.removeAll(_hexagons);

		// if we have hexes left over we need to try to create new territories
		while (originalHexes.size() > 0) {
			System.out.println("Create a new Territory");
			hex = (Hexagon) originalHexes.remove(0);
			Territory territory = new Territory(_board, hex);
			originalHexes.removeAll(territory._hexagons);
			if (territory.checkValid()) {
				_board.add(territory);
			}
		}

		// now we need to check whether we are still valid
		checkValid();

	}

	final boolean checkValid() {
		if (_hexagons.size() < 2) {
			// territory is too small to survive
			for (int i = 0; i < _hexagons.size(); i++) {
				Hexagon h = (Hexagon) _hexagons.get(i);
				h.reset();
			}
			_current = false;
			_hexagons.clear();
			_townLocation = null;
			_board.remove(this);
			return false;
		}
		return true;
	}

	final boolean isValid() {
		return _hexagons.size() > 1;
	}

}
