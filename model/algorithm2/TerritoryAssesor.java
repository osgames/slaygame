package model.algorithm2;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * TerritoryAssesor.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * This class represents one players assessment of one territory
 */
final class TerritoryAssesor {

	// private final PlanTerritory _territory;
	// private final TerritoryAssessment _assessment;
	// private final Player _planOwner;
	//
	// public TerritoryAssesor(PlanTerritory t) {
	// _territory = t;
	// _planOwner = t.getOwner();
	// _assessment = new TerritoryAssessment(_planOwner);
	//
	// }
	//
	// /**
	// * Draw a collection of hexes and their boundaries
	// *
	// * @param graphics
	// * @param hexs
	// */
	// final void draw(Graphics graphics) {
	// _assessment.drawPlan(graphics);
	// }
	//
	// final int calculate() {
	// int value = _assessment.calculate(_territory.getHexagons());
	// // _assessment.report();
	// return value;
	// }
	//
	// // final void drawPlan(Graphics graphics) {
	// // for (int i = 0; i < _safeHexes.size(); i++) {
	// // PlanHex hexagon = (PlanHex) _safeHexes.get(i);
	// // hexagon.drawSafe(graphics, 0);
	// // }
	// //
	// // for (int i = 0; i < _frontlineHexes.size(); i++) {
	// // PlanHex hexagon = (PlanHex) _frontlineHexes.get(i);
	// // hexagon.drawFrontline(graphics, 0);
	// // }
	// //
	// // for (int i = 0; i < _enemies.size(); i++) {
	// // PlanHex hexagon = (PlanHex) _enemies.get(i);
	// // hexagon.drawEnemy(graphics, 0);
	// // }
	// //
	// // for (int i = 0; i < _seaHexes.size(); i++) {
	// // PlanHex hexagon = (PlanHex) _seaHexes.get(i);
	// // hexagon.draw(graphics);
	// // }
	// //
	// // for (int i = 0; i < _unownedHexes.size(); i++) {
	// // PlanHex hexagon = (PlanHex) _unownedHexes.get(i);
	// // hexagon.drawUnowned(graphics, 0);
	// // }
	// // ArrayList _hexagons = _territory.getHexagons();
	// //
	// // _drawBorder(graphics, _hexagons);
	// // _drawBorder(graphics, _enemies);
	// // _drawBorder(graphics, _seaHexes);
	// // _drawBorder(graphics, _unownedHexes);
	// // }
	//
	// final int getFrontlineLength() {
	// return _assessment.getFrontlineLength();
	// }
	//
	// final void calculateConquest() {
	// int currentValue = _assessment.value();
	// TerritoryAssessment assess = new TerritoryAssessment(_planOwner);
	// ArrayList hexs = new ArrayList(_territory.getHexagons());
	// ArrayList targets = new ArrayList(_assessment.getEnemies());
	// targets.addAll(_assessment.getUnownedHexes());
	// for (int i = 0; i < targets.size(); i++) {
	// PlanHex enemy = (PlanHex) targets.get(i);
	// HexState savedState = enemy.getState();
	// hexs.add(enemy);
	// enemy.setPlanOwner(_planOwner);
	// int value = assess.calculate(hexs) - currentValue;
	//
	// enemy.setIntData(value);
	//
	// hexs.remove(enemy);
	// enemy.setState(savedState);
	// }
	// }

}
