package model;

public class Settings {

	public static boolean expert = true;
	public static boolean allowTownMove = false;
	public static boolean allowRetire = false;
	public static boolean cumulativeDefence = false;
	public static boolean rememberMap = false;

}
