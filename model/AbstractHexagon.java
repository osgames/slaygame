package model;

import java.awt.Point;
import java.awt.Polygon;
import java.util.ArrayList;

/* 
 * Copyright 2010 Phill van Leersum
 * 
 * AbstractHexagon.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Class to remove some of the infrastructure methods from Hexagon
 */
public abstract class AbstractHexagon extends Fn {
	static final int DOWNLEFT = 3;
	static final int DOWNRIGHT = 2;
	static final int LEFT = 4;
	static final int RIGHT = 1;
	static final int UPLEFT = 5;
	static final int UPRIGHT = 0;

	protected final Board _board;
	protected final int _column;
	protected boolean _isLand;
	protected final Hexagon[] _neighbours = new Hexagon[6];
	protected int _radius;
	protected final int _row;
	protected int _x;
	protected int[] _xPts;
	protected int _y;
	protected int[] _yPts;

	public AbstractHexagon(Board board, int row, int column) {
		super();
		_board = board;
		_row = row;
		_column = column;
	}

	/**
	 * Return true if the point is within the hexagon, false otherwise.
	 * 
	 * @param pt
	 * @return
	 */
	protected final boolean contaimsPoint(Point pt) {
		Polygon polygon = new Polygon(_xPts, _yPts, 6);
		boolean isPoint = polygon.contains(pt);
		return isPoint;
	}

	protected final Hexagon getNeighbour(int index) {
		Hexagon neighbour = null;
		if (index >= -1 && index < _neighbours.length) {
			neighbour = _neighbours[index];
		}
		return neighbour;
	}

	public final Hexagon[] getNeighbours() {
		return _neighbours;
	}

	public final boolean isAdjacentTo(ArrayList group) {
		boolean is = false;
		for (int i = 0; i < _neighbours.length & !is; i++) {
			Hexagon neighbour = _neighbours[i];
			if (null != neighbour) {
				is = group.contains(neighbour);
			}
		}
		return is;
	}

	public final boolean isLand() {
		return _isLand;
	}

	public final boolean isSea() {
		return !_isLand;
	}

	protected final void setAsLand() {
		_isLand = true;
	}

	protected final void setAsSea() {
		_isLand = false;
	}

	protected final void setCoordinates(int xc, int yc, int r) {
		_x = xc;
		_y = yc;
		_radius = r;
		int h = _radius / 2;
		// Note we have SEVEN points in each array. The first point is repeated
		// at the end, so that we can easily draw individual borders.
		_xPts = new int[] { _x, _x + r, _x + r, _x, _x - r, _x - r, _x };
		_yPts = new int[] { _y - r, _y - h, _y + h, _y + r, _y + h, _y - h, _y - r };
	}

	protected final void setDownLeft(Hexagon h) {
		_neighbours[DOWNLEFT] = h;
	}

	protected final void setDownRight(Hexagon h) {
		_neighbours[DOWNRIGHT] = h;
	}

	protected final void setLeft(Hexagon h) {
		_neighbours[LEFT] = h;
	}

	protected final void setRight(Hexagon h) {
		_neighbours[RIGHT] = h;
	}

	protected final void setUpLeft(Hexagon h) {
		_neighbours[UPLEFT] = h;
	}

	protected final void setUpRight(Hexagon h) {
		_neighbours[UPRIGHT] = h;
	}

	public final Board getBoard() {
		return _board;
	}

}
