package model;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Fn.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public abstract class Fn implements GeneralConstants {
	// 32 bit integer used to indicate nature of occupant
	// 0b00000000 00000000 MMMMMMMM MMMVTTTT
	// T = Type bit V = Moveable M = Money bits

	private static final int MONEY_MASK = 0x0000FFE0;
	private static final int MONEY_SHIFT = 5;
	private static final int MOVEABLE = 0x00000010;
	private static final int NOT_MOVEABLE = 0xFFFFFFEF;
	private static final int TYPE_MASK = 0x0000000F;
	private static final int MAXIMUM_MONEY = 1000;

	public static final int getType(int type) {
		return type & TYPE_MASK;
	}

	public static final boolean isCastle(int type) {
		return getType(type) == CASTLE;
	}

	public static final boolean isSoldier(int type) {
		switch (getType(type)) {
			case PRIVATE:
			case MAJOR:
			case CAPTAIN:
			case COLONEL:
				return true;
		}
		return false;
	}

	public static final boolean isTown(int type) {
		return getType(type) == TOWN;
	}

	final int getMoney(int type) {
		int money = 0;
		if (isTown(type)) {
			money = (type & MONEY_MASK) >> MONEY_SHIFT;
		}
		return money;
	}

	final boolean isGrave(int type) {
		return getType(type) == GRAVE;
	}

	final boolean isMoveable(int occ) {
		int type = getType(occ);
		boolean isMoveable = false;
		switch (type) {
			case NONE:
				break;
			case TOWN:
				isMoveable = Settings.allowTownMove;
				break;
			default:
				isMoveable = (occ & MOVEABLE) == MOVEABLE;
				break;
		}
		return isMoveable;
	}

	final int setMoney(int type, int money) {
		if (isTown(type)) {
			if (money > MAXIMUM_MONEY) {
				money = MAXIMUM_MONEY;
			}
			money = money << MONEY_SHIFT;
			// type = type & MONEY_NOT_MASK;
			type = TOWN + money;
		}
		return type;
	}

	final int setMoveable(int occ, boolean moveable) {
		int type = occ & NONE;
		switch (type) {
			case NONE:
			case GRAVE:
			case CASTLE:
				break;
			case TOWN:
			default:
				if (moveable) {
					occ = occ | MOVEABLE;
				} else {
					occ = occ & NOT_MOVEABLE;
				}
				break;
		}
		return occ;
	}
}
