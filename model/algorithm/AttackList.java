package model.algorithm;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;

import model.Hexagon;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * AttackList.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Holder for a list of attacks (duh)
 * 
 * @author Phill van Leersum
 * 
 */
public class AttackList {
	private int _value = -1;
	private final ArrayList _attacks;

	public AttackList() {
		super();
		_attacks = new ArrayList();
	}

	public AttackList(AttackList attackList, int value) {
		_attacks = new ArrayList(attackList._attacks);
		_value = value;
	}

	public final int getValue() {
		return _value;
	}

	public final void setValue(int value) {
		_value = value;
	}

	public final Attack add(int attacker, Hexagon location) {
		Attack attack = new Attack(attacker, location);
		_attacks.add(attack);
		return attack;
	}

	public String toString() {
		StringWriter sw = new StringWriter();
		PrintWriter pWriter = new PrintWriter(sw);
		pWriter.print("Value = ");
		pWriter.print(_value);
		pWriter.print("; [");
		for (int i = 0; i < _attacks.size(); i++) {
			Attack attack = (Attack) _attacks.get(i);
			pWriter.write(attack.toString());
			pWriter.write("; ");
		}

		pWriter.write("]");

		return sw.toString();
	}

	public int getSize() {
		return _attacks.size();
	}

	/**
	 * @return
	 */
	public Iterator getAttacks() {
		return _attacks.iterator();
	}

}
