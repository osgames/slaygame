/**
 * 
 */
package model.algorithm;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Timer.java is part of Phill van Leersum's SlayGame program.
 * 
 * SlayGame is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * SlayGame is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * SlayGame. If not, see http://www.gnu.org/licenses/.
 * 
 */
/**
 * @author Phill van Leersum Polling timer - detects expiry of elapsed time when
 *         polled.
 */
public class Timer {
	private long _timeout = 5000;

	private long _startTime;

	private boolean _triggered = false;

	private int divider = 0;

	private static final int DIVIDER = 10;

	public final void start(final long time) {
		_startTime = System.currentTimeMillis();
		_timeout = time + _startTime;
		_triggered = false;
	}

	public final boolean check() {
		if (!_triggered) {
			divider = divider + 1;
			if (divider > DIVIDER) {
				divider = 0;
				long time = System.currentTimeMillis();
				_triggered = (time > _timeout);
				if (_triggered) {
					System.out.println("Timeout triggered");
				}
			}
		}
		return _triggered;
	}

	public final long elapsed() {
		return System.currentTimeMillis() - _startTime;
	}

}
