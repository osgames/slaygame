package model;

import java.util.ArrayList;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Random.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class Random {
	private static java.util.Random _random = new java.util.Random(System.currentTimeMillis());

	public static final Object get(ArrayList list) {
		Object o = null;
		if (list.size() == 1) {
			o = list.get(0);
		} else if (list.size() > 1) {
			int r = positiveInteger(list.size());
			o = list.get(r);
		}
		return o;
	}

	public static final int positiveInteger(int mod) {
		int r = _random.nextInt(mod);
		r = Math.abs(r);
		return r;
	}

	public static final long nextLong() {
		long r = _random.nextLong();
		return r;
	}

	public static final Object remove(ArrayList list) {
		Object o = null;
		if (list.size() == 1) {
			o = list.remove(0);
		} else if (list.size() > 1) {
			int r = positiveInteger(list.size());
			o = list.remove(r);
		}
		return o;
	}

	public static final void reseed() {
		_random.setSeed(System.currentTimeMillis());
	}

	public static final void seed(long seed) {
		_random.setSeed(seed);
	}

	/**
	 * 
	 */
	public static void standardise() {
		_random.setSeed(123456);
	}

}
